var mqtt = require('mqtt')
var json2xml = require('json2xml')
var client = mqtt.connect('mqtt://localhost:8080')

var topic = "cavailability"
var message = "{'Available': 'YES', 'Spots': '2', 'Duration': '30minutes'}"

const fs = require('fs')

/*
var data = '{"Data":{"SOM":{"Tab":[{"Values":{"ExpandedValues":null,"ID":"msorgrole"},"ID":"OrgRole"},{"Values":{"ExpandedValues":null,"ID":"msorg"},"ID":"Organization"}]}}}';
*/

var data = '{"Data":{"SOM":{"Tab":[{"Values":{"Available": "YES","Spots":"2","Duration":"30minutes"}}]}}}'

var jsonObj = JSON.parse(data)
var message = json2xml(jsonObj)
console.log(message)

client.on('connect', () => {
  setInterval( () => {
    client.publish(topic, message)
    console.log('message sent', message)
  }, 5000)
})