# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repositofy is for the tasks assignes in assignment two. It's to demonstrate knowledge on IOT and the broker-subscriber relationship.
We have a broker that sends information to a set of subscribers. Since we do not have a physical sensor that would provide data for the broker, the messages will be done statically.

For a case, we have a gym equipment sensor, that sends the usage-status of a workout-equipment(s). This will have to be done periodically (since we do not have a sensor that realistically updates). Practically this would mean status on e.g. benchpress/pullup bar is taken/available.

Version: 0.0.1
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###


-- If you get error "SchemaError: Expected `schem`...", comment out the if-statement as described here: https://stackoverflow.com/questions/64189045/node-js-mosca-broker-error-expected-schema-to-be-an-object-or-boolean .


* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Ians
* Smesky


### Checklist
* Functioning broker/subscriber *done*
* Functioning database connection *done*
* Functioning Multiple subscribers: *done, with multiple topics*
* Complete IOT scenario: *done*
	* With MQTT-protocol: *done*
* SenML/JSON payload: *done*
* SenML/XML payload: *done*
* SenML/EXI payload

