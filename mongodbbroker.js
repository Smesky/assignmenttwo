var mosca = require('mosca')
var settings = {port:8080}
var broker = new mosca.Server(settings)

var mongc = require('mongodb').MongoClient
var url = "mongodb+srv://user1:Password3@learner.okwiz.mongodb.net/assign2?retryWrites=true&w=majority"

broker.on('ready', () => {
  console.log('MongoBroker is ready!')
})

broker.on('published', (packet) => {
  message = packet.payload.toString()
  console.log(message)
  mongc.connect(url, (error, client) => {
    var myCol = client.db('assign2').collection('brokersz')
    myCol.insertOne({
      message: message
    }, () => {
      console.log('Data is now saved to MongoDB')
      client.close()
    })
  })
})
